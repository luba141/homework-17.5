#include <iostream>

class Vector
{
private:
    double x, y, z;
    public:
        Vector(): x(0), y(0), z(0)
        {
            std::cout << "Vector zero created\n";
        }

        Vector(double X, double Y, double Z) : x(X), y(Y), z(Z)
        {
            std::cout << "Vector "<< x << " " << y << " " << z << " created\n";
        }

        double GetLength()
        {
            return sqrt(x * x + y * y + z * z);
        }

};

int main()
{
    Vector v1, v2(1, 2, 3), v3(-1, 4, 6);
    std::cout << "L1 = " << v1.GetLength() << '\n';
    std::cout << "L2 = " << v2.GetLength() << '\n';
    std::cout << "L3 = " << v3.GetLength() << '\n';
}
